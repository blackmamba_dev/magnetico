Reqs:
1.Python3
2.Pip3
3.Gevent which is installed via: pip3 install gevent --user

Instructions:
1. Install magneticod by going to magnetico/magnetico-0.6.0/magneticod/ and type this command: sudo python3 setup.py install
2. Install magneticow by going to magnetico/magnetico-0.6.0/magneticow/ and type this command: sudo python3 setup.py install
3. Go and edit magnetico/magnetico-0.6.0/magneticow/build/lib/magneticow/__main__.py and find gevent.wsgi and change everywhere to gevent.pywsgi
4. Install magneticow again by repeating step 2.
5. Run by running magneticod and magneticow in terminal
